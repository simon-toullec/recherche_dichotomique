# Rechercher la position de ma valeur depuis un tableau trié

- **Etant donné que**

  ```json
  [1, 2, 3]
  ```

  **Quand** je cherche depuis `/` `POST` la valeur 1

  ```json
  {
    "value": 1
  }
  ```

  **Alors** l'api me retourne

  ```json
  {
    "position": 0
  }
  ```

- **Etant donné que**

  ```json
  [1, 2]
  ```

  **Quand** je cherche depuis `/` `POST` la valeur 1

  ```json
  {
    "value": 1
  }
  ```

  **Alors** l'api me retourne

  ```json
  {
    "position": 0
  }
  ```

- **Etant donné que**

  ```json
  [1, 2, 3]
  ```

  **Quand** je cherche depuis `/` `POST` la valeur 3

  ```json
  {
    "value": 3
  }
  ```

  **Alors** l'api me retourne

  ```json
  {
    "position": 2
  }
  ```

- **Etant donné que**

  ```json
  [1, 2, 3]
  ```

  **Quand** je cherche depuis `/` `POST` la valeur 2

  ```json
  {
    "value": 2
  }
  ```

  **Alors** l'api me retourne

  ```json
  {
    "position": 1
  }
  ```
