Ajouter DATABASE_URL="postgresql://symfony:ChangeMe@127.0.0.1:5432/app?serverVersion=13&charset=utf8" dans le .env

Ajouter le dossier /docker et ses fichiers
Ajouter le dockerignore
Lancer le build: $docker build . -f ./docker/Dockerfile -t api-movie-cqrs-v2

Modifier dans le fichier docker-compose.yaml app: image... par le nom récupérer de la génération du build.

Ouvrir le fichier ./docker/docker-compose.yml clic droit => Compose up