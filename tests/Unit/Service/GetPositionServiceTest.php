<?php

namespace App\Tests\Unit\Service;

use App\Model\Payload;
use App\Service\GetPositionService;
use Generator;
use PHPUnit\Framework\TestCase;

class GetPositionServiceTest extends TestCase
{
    private GetPositionService $getPositionService;
    public function setUp(): void
    {
        $this->getPositionService = new GetPositionService();
    }

    /**
     * @dataProvider provider
     */
    public function testExecute(Payload $payload, int $expected): void
    {
        $actual = $this->getPositionService->execute($payload);
        static::assertSame(
            $actual,
            $expected
        );
    }

    public function provider(): Generator
    {
        $expected = 0;
        $payload = new Payload(
            1,
            [1, 2, 3]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 2;
        $payload = new Payload(
            3,
            [1, 2, 3]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 1;
        $payload = new Payload(
            2,
            [1, 2, 3]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 0;
        $payload = new Payload(
            1,
            [1, 2, 3, 4]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 1;
        $payload = new Payload(
            2,
            [1, 2, 3, 4]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 2;
        $payload = new Payload(
            3,
            [1, 2, 3, 4]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 3;
        $payload = new Payload(
            4,
            [1, 2, 3, 4]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 2;
        $payload = new Payload(
            20,
            [2, 6, 20, 33, 34, 37, 46, 50, 51, 52, 80]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 8;
        $payload = new Payload(
            51,
            [2, 6, 20, 33, 34, 37, 46, 50, 51, 52, 80]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 8;
        $payload = new Payload(
            51,
            [2, 6, 20, 33, 34, 37, 46, 50, 51, 52]
        );
        yield [
            $payload,
            $expected
        ];
        $expected = 2;
        $payload = new Payload(
            20,
            [2, 6, 20, 33, 34, 37, 46, 50, 51, 52]
        );
        yield [
            $payload,
            $expected
        ];
    }
}