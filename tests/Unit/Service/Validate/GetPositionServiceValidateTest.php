<?php

namespace App\Tests\Unit\Service\Validate;

use App\Model\Payload;
use App\Service\Validate\GetPositionServiceValidate;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class GetPositionServiceValidateTest extends TestCase
{
    public function testThrowErrorIfValueNotFound(): void
    {
        $this->expectException(BadRequestException::class);
        $payload = new Payload(-1, [1, 2, 3]);
        GetPositionServiceValidate::throwErrorIfValueNotFound($payload);
    }
}