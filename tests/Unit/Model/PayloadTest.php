<?php

namespace App\Tests\Unit\Model;

use App\Model\Payload;
use PHPUnit\Framework\TestCase;

class PayloadTest extends TestCase
{
    public function testGetters(): void
    {
        $payload = new Payload(
            1,
            [1, 2, 3]
        );
        static::assertSame(
            [1, 2, 3],
            $payload->getListValue()
        );
        static::assertSame(
            1,
            $payload->getValue()
        );
    }
}