<?php

namespace App\Model;

class Payload
{
    public function __construct(
        private int $value,
        private array $listValue
    ) {
    }

    public function getListValue(): array
    {
        return $this->listValue;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}