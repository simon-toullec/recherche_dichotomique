<?php

namespace App\Service\Validate;

use App\Model\Payload;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class GetPositionServiceValidate
{
    public static function throwErrorIfValueNotFound(Payload $payload): void
    {
        throw new BadRequestException(
            sprintf(
                'This value %s is not found in %s',
                $payload->getValue(),
                json_encode($payload->getListValue())
            )
        );
    }
}