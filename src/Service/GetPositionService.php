<?php
namespace App\Service;

use App\Model\Payload;
use App\Service\Validate\GetPositionServiceValidate;

class GetPositionService
{
    private int $startPosition;
    private int $endPosition;
    public function execute(Payload $payload): int
    {
        $this->startPosition = 0;
        $this->endPosition = count($payload->getListValue());
        while (count($payload->getListValue()) > 1) {
            $middlePositionOfList = $this->getMiddlePositionOfList(
                $payload
            );
            if ($payload->getValue() < $payload->getListValue()[$middlePositionOfList]) {
                $this->endPosition = $middlePositionOfList;
            }
            if ($payload->getValue() > $payload->getListValue()[$middlePositionOfList]) {
                $this->startPosition = $middlePositionOfList;
            }
            if ($payload->getValue() === $payload->getListValue()[$middlePositionOfList]) {
                return $middlePositionOfList;
            }
        }

        GetPositionServiceValidate::throwErrorIfValueNotFound($payload);
    }

    private function getMiddlePositionOfList(Payload $payload): int
    {
        $positionRoundedBySup = $this->startPosition + floor(($this->endPosition - $this->startPosition) / 2);
        if (
            $this->isOdd($positionRoundedBySup) ||
            $payload->getValue() >= $payload->getListValue()[$positionRoundedBySup]
        ) {
            return $positionRoundedBySup;
        }

        return $positionRoundedBySup - 1;
    }

    private function isOdd(int $value): bool
    {
        return 0 !== $value % 2;
    }
}