<?php

namespace App\Controller;

use App\Model\Payload;
use App\Service\GetPositionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class GetController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private GetPositionService $getPositionService
    ) {
    }
    #[Route('/', methods: ['POST'])]
    public function get(Request $request): JsonResponse
    {
        $payload = $this->serializer->deserialize($request->getContent(), Payload::class, 'json');

        return new JsonResponse($this->getPositionService->execute($payload));
    }
}