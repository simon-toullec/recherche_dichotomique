# Protocole

## Le ticket doit comporter une description globale, elle se met à la place de l'utilisateur.

Exemple:

**En tant** qu'utilisateur  
**J'ai besoin de** renseigner mon Directeur  
**Afin de** pouvoir l'enregistrer dans ma base

On peut en indiquer plusieurs si plusieurs cas s'apprêtent.

## Le ticket doit comporter la liste des UATs

Exemple:

```gherkin
## Création d'un `on_call` du `user.role` = 'consultant' une nuit en semaine

- **Depuis** `/module/on-call`
  **Etant donné que** je suis `user.role` == 'consultant'
  **Quand** je renseigne la `on_call.start_date` = '10/04/2023 18:00:00'
  **Et que** je renseigne la `on_call.end_date` = '11/04/2023 07:59:59'
  **Et que** je dépose le `on_call.supporting_document` = './document/idConsultant/hno/nomDocument.xlsx'
  **Et que** je que `on_call.user_team` = 'user1'
  **Et que** `on_call.on_call_status_type.name` = 'pending'
  **Et que** je crée
  **Alors** un `on_call` est créé `on_call.start_date` = '10/04/2023 18:00:00', `on_call.end_date` = '10/04/2023 07:59:59', `on_call.user_team` = 'user1', `on_call.on_call_status_type.name` = 'pending', `on_call.factor.name` = 'nuit'
```

## Chaque titre d'UAT doit commencer par un nom

Exemple: Création de..., Modification de ...

### Les mots clefs:

- Depuis = Doit exprimer l'endroit où se trouve l'utilisateur sur l'application
- Etant donné que = Doit exprimer un contexte, les données déjà enregistrées, le rôle de l'utilisateur etc...
- Quand = Doit exprimer les actions que réalise l'utilisateur
- Alors = Doit exprimer le résulat du Quand

### Les caractères

- \*\* => Pour définir un mot clef
- ` => Pour définir un terme technique: champ en bdd, table en bdd, url. Tout ce qui est utilisé dans le code.
- ' => Pour définir une valeur à affecter
- = => Pour définir une affectation d'une valeur à un champ
- == => Pour définir une égalité

### Les verbes:

Il ne faut pas utiliser de synonyme, un seul mot doit exprimer une seule chose. Par exemple, on ne peut pas écrire "créer" dans une première UAT puis "enregistrer" dans la deuxième. Il est donc nécessaire de définir une convention.

- Modifier = La donnée est modifiée
- Créer = Un nouvel objet est créé et enregitré dans la base
- Renseigner = L'utilisateur effectue une saisie clavier
- Supprimer = L'objet est supprimé de la base
- Archiver = L'objet est déplacer dans une table \_history

## Les données doivent représenter des cas réels

- Pas de valeur toto, etc...
- Les dates doivent correspondre aux dates courantes
- Les personnes doivent exister si possible
- Les données et relations doivent être cohérentes

## Conception

Cette approche permet de réaliser la phase de conception par l'analyse. Plus besoin de réaliser le diagramme de classe puisque les champs et tables seront définis dans l'UAT.

Les champs et tables exprimés dans notre UAT sont nommé en snake_case car ils représenteront les champs et tables de notre bdd. Notre bdd sera en snake_case, notre code en camelCase.

Exemple: `director_type.created_at`. La table est `director_type` et son champs `created_at`.
