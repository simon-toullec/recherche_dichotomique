Related to #1

### Git checks
* [ ] Merge request linked to the related issue ( see the related Issue section above)
* [ ] Branch named properly (<type>/<issue_id><issue_name>) GIT convention
* [ ] Branch rebased on source branch
* [ ] Commit message format is respected ([#<issue_id>] <type>(<scope>) : <short_summary>)
* [ ] Commit list cleaned (git rebase -i)

### Code checks
* [ ] Code in english, commented, strongly typed and explicit

### Tests checks
* [ ] PHPUnit tests added and validLaunch security vulnerability analysis
* [ ] Test coverage must be positive
* [ ] No PHPCs security vulnerability
* [ ] No PHPMD errors
* [ ] No PHPSTAN errors

### Deploy checks
* [ ] (If applicable) Gitlab CI, docker conf updated

### Documentation checks
* [ ] README / CHANGELOG / Gitlab Wiki updated
* [ ] (If applicable) Environnement variables was added to CHANGELOG and README 
* [ ] (If applicable) Update OpenApi doc
* [ ] Add demo scenario on issue

### Pre-merge
* [ ] All the items in the checklist above been reviewed
