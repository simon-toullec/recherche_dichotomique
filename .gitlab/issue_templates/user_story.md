# User Story

## Informations
- Application / Module:
- API version (ex: X.Y.Z)

## Description
(Thanks to describe the goal in as much detail as possible)

## Mock-up

## Example

## User acceptance test

(describe the behaviour on the endpoint)

[ ] POST /path {"payload"}: 200

## Technical information
/story_points
/label ~"type::user_story"
/label ~"workflow::definition_proposal"
/label ~"component::api"
