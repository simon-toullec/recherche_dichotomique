## Informations
- Application / Module:
- API version (ex: X.Y.Z)

## Description
(Thanks to describe the goal in as much detail as possible)

## Breeding method
(How to reproduce the bug)

## Faulty behaviour
(Describe the current behaviour)

## Expected behaviour
(Describe the expected behaviour)

## Screenshot
(Add a maximum of informations log/code/screenshot)

## Proposed correction
(Idea to correct the bug)

## Technical information
/story_points
/label ~"type::bug"
/label ~"component::api"
/label ~"workflow::bug_open"
