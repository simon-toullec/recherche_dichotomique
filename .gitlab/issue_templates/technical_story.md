## Informations
- Application / Module:
- API version (ex: X.Y.Z)

## Description
(Thanks to describe the goal in as much detail as possible)

## Screenshot
(Add a maximum of informations log/code/screenshot)

## Ressources
(link, doc, post on forum which can help for this issue)

## User acceptance test

(describe the behaviour on the endpoint)

[ ] POST /path {"payload"}: 200

## Technical information
/story_points
/label ~"type::technical"
/label ~"workflow::definition_proposal"
/label ~"component::api"
