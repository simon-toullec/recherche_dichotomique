| TITLE                           | DESCRIPTION                                 | COLOR   |
| ------------------------------- | ------------------------------------------- | ------- |
| component::api                  | Correspond à la sous-partie du projet api   | #1b0fff |
| component::ci/cd                | Correspond à la sous-partie du projet ci/cd | #1b0fff |
| component::tests                | Correspond à la sous-partie du projet tests | #1b0fff |
| environnement::prod             |                                             | #cd5b45 |
| environnement::pprod            |                                             | #a6ffb4 |
| module::director                |                                             | #156abf |
| module::login                   |                                             | #156abf |
| module::movie                   |                                             | #156abf |
| priority::high                  |                                             | #dc143c |
| priority::low                   |                                             | #dc143c |
| severity::critical              |                                             | #ff0000 |
| severity::low                   |                                             | #ff0000 |
| type::epic                      |                                             | #9916d8 |
| type::feature                   |                                             | #9916d8 |
| type::bug                       |                                             | #9916d8 |
| type::technical                 |                                             | #9916d8 |
| type::user_story                |                                             | #9916d8 |
| workflow::bug_open              |                                             | #d17bdb |
| workflow::build_to_do           |                                             | #d17bdb |
| workflow::build_dev_in_progress |                                             | #d17bdb |
| workflow::build_merged          |                                             | #d17bdb |
